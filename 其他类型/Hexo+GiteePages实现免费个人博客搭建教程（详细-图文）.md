本人作为一个屌丝程序员，**年少无为，卖马为生**，买不起服务器，买不起域名，但是又想拥有属于自己的博客网站，那就只有通过技术来实现这一切了。先上成果：[点击](http://z77z.oschina.io/)，现在我把我自己搭建博客的过程共享出来，只要你按照步骤一步步走下去，一定会搭建成功，如果大家在搭建过程中遇到什么问题，欢迎在我的博客评论区留言，也欢迎大神进来，教我做码，带我装逼，带我飞。**[博客地址](http://z77z.oschina.io/)**，JUST DO IT(`离开舒适区`)。
----------

使用工具介绍
------

 1. `码云 Pages`：码云 Pages 是一个免费的静态网页托管服务，您可以使用码云 Pages 托管博客、项目官网等静态网页。**这样就不用购买服务器和域名了**，如果您使用过 `Github Pages` 那么您会很快上手使用码云的Pages服务。这里使用码云上的Pages 而不是用`Github Pages`，主要原因就是在国内没有VPN的话。。。你懂得！还有就是支持国产！链接：[https://git.oschina.net/](https://git.oschina.net/)

 2. `Hexo`：`Hexo` 是一个快速、简洁且高效的博客框架。`Hexo` 使用 `Markdown`（或其他渲染引擎）解析文章，在几秒内，即可利用靓丽的主题生成静态网页。使用者只需要专注于写博客而不需要关注网站是如何生成的。而且还有丰富的博客主题可以选择！链接：[https://hexo.io/zh-cn/](https://hexo.io/zh-cn/)（ps：这是国外网站，翻墙吧少年，需要`自由门` 翻墙软件可以在博客中联系我哦）


搭建环境
----

 **Node.js安装。**
 

> 参考：[http://www.runoob.com/nodejs/nodejs-install-setup.html](http://www.runoob.com/nodejs/nodejs-install-setup.html)（ps：如果没有VPN的用户最好切换一下npm的源，不然后续的步骤可能会让你想砸电脑，安装完后，在命令窗口执行下面代码）

```
npm config set registry http://registry.cnpmjs.org     #切换npm为淘宝镜像
```

 **Git安装。**

 

> 参考[http://www.runoob.com/git/git-install-setup.html](http://www.runoob.com/git/git-install-setup.html)

**Hexo安装。**（ ps：安装完Node.js和Git之后不需要配置些什么，只需要安装成功就是了，如果想明白原理的话可以深入的学习。）

 如果您的电脑中已经安装上述必备程序，那么恭喜您！接下来只需要使用 npm 即可完成 Hexo 的安装。打开命令窗口输入下面代码：
 

```
npm install -g hexo-cli
```

安装成功后输入`hexo` 如果得到下面这个结果，恭喜你！安装成功！


![Hexo安装成功后效果](https://oscimg.oschina.net/oscnet/34aefcf509bbd36584b00e25e06eb4b0fcb.jpg)
 
Hexo的常用命令说明：

> 参考：[https://hexo.io/zh-cn/docs/commands.html](https://hexo.io/zh-cn/docs/commands.html)


本地运行Hexo
--------

安装 Hexo 完成后，请依次执行下列命令，Hexo 将会在指定文件夹中新建所需要的文件。将`<folder>` 替换成项目存放的文件夹目录，
```
hexo init <folder>
cd <folder>
npm install
hexo generate       
```
新建完成后，指定文件夹的目录如下：
```
.
├── .deploy       #需要部署的文件
├── node_modules  #Hexo插件
├── public        #生成的静态网页文件
├── scaffolds     #模板
├── source        #博客正文和其他源文件, 404 favicon CNAME 等都应该放在这里
|   ├── _drafts   #草稿
|   └── _posts    #文章
├── themes        #主题
├── _config.yml   #全局配置文件
└── package.json
```

 进行到这步后就可以先在本地运行下，看看效果了。执行下面命令：
 

```
cd <folder>         #切换到项目目录下
npm install   		#install before start blogging
hexo server         #运行本地服务
```

浏览器输入[http://localhost:4000](http://localhost:4000)就可以看到效果。如下：

![默认配置运行效果](https://oscimg.oschina.net/oscnet/194fc0e570a1c0f3fabf56df6da53f3bad7.jpg)

修改Hexo的主题
---------

当然，上面运行的只是Hexo官方默认的主题配置效果，想要个性一点？，下面我们就将他改造成自己想要的个性主题。

Hexo的主题都是一些前端民间高手写的模版，可以在官方收录的主题中去挑选：[https://hexo.io/themes/](https://hexo.io/themes/)

看上一个主题后点击下图位置，进入下载主题文件。如下图：

![](https://oscimg.oschina.net/oscnet/6c86f18820b8d8b7d099a738cb1191ca305.jpg)

![](https://oscimg.oschina.net/oscnet/deadc4ed534568c0dfb6d5c1a743fe05083.jpg)

主题下载完成后，将主题文件解压到Hexo项目的thems文件夹下面（ps:我这里的项目目录是在桌面的Hexo文件夹里面。）

![](https://oscimg.oschina.net/oscnet/8896feca7beada235e01a05fc821598a683.jpg)

接下来就是修改Hexo的配置文件`_config.yml`，将里面`theme` 对应的值改为之前下载的主题的文件夹名字，本文里面下载的主题文件夹名字为`hexo-theme-smackdown-master`。

修改前

![修改前](https://oscimg.oschina.net/oscnet/cca03fe487fdc3aa2908248bfc27d3b39e7.jpg)

修改后

![修改后](https://oscimg.oschina.net/oscnet/734a4053befa1e4f1826315f0fcac29694e.jpg)

*注意：这里“：”后面必须要有一个空格，而且这个空格要在英文输入法下，不然会报一些稀奇古怪的错。*

按照之前步骤本地重新运行后，如下图：

![改变主题后效果](https://oscimg.oschina.net/oscnet/14aa543d9d0af2eba9162a0a9a03ed1a88d.jpg)

关于Hexo中`_config.yml` 文件的其他配置，大家可以参考官方的文档，这里就不多赘述了：

>文档链接 [https://hexo.io/zh-cn/docs/configuration.html](https://hexo.io/zh-cn/docs/configuration.html)

下载的每个主题中，有一个配置文件，名字也叫`_config.yml`，这里大家千万不要和之前Hexo根目录下的`_config.yml`搞混淆了，这里面配置的主要是些与主题相关的东西，比如一些文章阅读量，多说插件，cnzz站长工具等等的配置信息。一般里面也都有注释，这里就不赘述了，不懂得可以在我的博客问我。


一些主题在GitHub上面也都有主题安装的一些文档，写的都很详细。推荐一个主题的文档，结合官方的文档看完之后，基本上也就明白了。

>推荐一个主题的文档[https://github.com/yscoder/hexo-theme-indigo/wiki](https://github.com/yscoder/hexo-theme-indigo/wiki)


编写博客文章
----

如果你使用过`MarkDown` 来写博客文章的话，接下来就简单多了，没使用过也没关系，推荐大家一个编辑工具`马克飞象` 链接：[https://maxiang.io/](https://maxiang.io/)，在编辑器里面写好文章后，复制或另存为.md文件, 与普通的.md文件不同   要在文件开头添加下面代码

```
title: #文章标题
date: #文章日期
tags: #文章标签
categories: #文章分类
---
```

在.md文件的开头添加上面代码，是为了让Hexo框架在生成网页的时候，设置相应的参数。例如下图所示：

![](https://oscimg.oschina.net/oscnet/d941379bd6c3bd3d35c056398b2b8eceb75.jpg)


将写好的.md文件放入Hexo项目的`source` 目录下的`_posts` 文件夹中，可以看到里面有个`hello-world.md` 文件，这就是默认的文章。

重新在本地启动项目，访问就可以看到之前添加的文章。

生成静态Html文件
----------

现在只能在本地启动项目然后通过本地地址访问博客网站，下面我们就可以利用Hexo生成静态Html，很简单，只需要在命令窗口执行下面代码：

```
cd <folder>         #切换到项目目录下
hexo generate       #生成静态文件到项目根目录的public文件夹中
```

发布静态Html文件到码云 Pages上
--------------------

注册一个码云帐号，并创建一个项目。如下图：

![](https://oscimg.oschina.net/oscnet/caf5ce87ec3c2d7c25cbfd759de4cbffd7d.jpg)


创建完项目后得到项目的Https的地址后面要用。地址获取如下图

![](https://oscimg.oschina.net/oscnet/a46b15011c91c9f3a6173aed43bc2234036.jpg)



这里借用一个插件来帮助我们完成代码上传的工作，安装 hexo-deployer-git。安装代码如下：

```
npm install hexo-deployer-git --save        #把public里面生成的文件推上到码云上。

```

配置项目根目录`_config.yml` 文件，修改`deploy` 的值，如下图：

修改前

![修改前](https://oscimg.oschina.net/oscnet/811b5dd44b1f130e13c8bcfa5942b5d0576.jpg)

修改后，注意`repo` 的地址是之前在码云上面创建醒目后获取的地址。

![修改后](https://oscimg.oschina.net/oscnet/3a22db64315c28c75b07c9954d42b04cd21.jpg)

修改完后在命令窗口执行下面命令：

```
cd <folder>         #切换到项目目录下
hexo deploy         #一键部署功能
```

**之后会弹出一个对话框，输入码云的帐号密码。**

部署成功之后，登录码云，查看之前创建的项目中出现了本地项目中`public` 文件夹中的文件，这时候代表之前的部署是成功的。

然后如下图，启动码云的pages功能：

![](https://oscimg.oschina.net/oscnet/47f20502211ef38ddeb2b590c636c511637.jpg)

访问链接：

![](https://oscimg.oschina.net/oscnet/b56729d5442f96c6ccbe0920378eddc7b0d.jpg)

这时候就看到之前和本地启动一样的效果了。博客部署完成，在这之后，只需要每次用马克飞象写好文章后，放入Hexo项目的`source` 目录下的`_posts` 文件夹中，在按照之前步骤更新博客就OK了。还可以随时切换博客的主题哦！