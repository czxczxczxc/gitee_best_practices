# DONTNET(Core)项目在码云中使用 Appveyor 持续集成与持续交付服务

## 前言
在软件开发中经常会提到 持续集成(Continuous Integration)（CI）和 持续交付(Continuous Delivery)（CD）这几个术语。

#### 持续集成（CI）
是在源代码变更后自动检测、拉取、构建和（在大多数情况下）进行单元测试的过程。

#### 持续交付（CD）
通常是指整个流程链（管道），它自动监测源代码变更并通过构建、测试、打包和相关操作运行它们以生成可部署的版本，基本上不需要任何人为干预。

熟悉了这两个概念后，我们以 [ **BootstrapAdmin** ](https://gitee.com/LongbowEnterprise/BootstrapAdmin) 项目为例详细介绍一下如何在码云平台上集成 CI&CD 服务。  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/115545_194216db_554725.png "CI&CD.png")   
如上图所示，集成 CI&CD 后，一切都会变得非常方便，当开发人员向码云仓库提交新的代码后，就会触发 Appveyor 持续集成服务器的 Build 操作，Build 完成后，还可以执行测试，测试通过以后，根据配置的不同，可以手动或自动部署到生产环境中，这一切都是自动的，对于我们开发人员来说只需要把代码提交到码云中的仓库就可以了。  

接下来我们步入正题，开始介绍自动化持续集成持续交付相关工具与软件的介绍   

## Appveyor 配置

随着SaaS的兴起，[AppVeyor](https://www.appveyor.com) 把持续集成搬到了云端，我们无需架设自己的CI服务器，只需注册一个账号，通过 Git 连上 AppVeyor 就可以了，Appveyor 是我找到的唯一既支持 DOTNET 又免费的服务提供商了。（还可以支持私有仓库哦！需要自己安装 AppVeyor Server）
下面以 [ **BootstrapAdmin** ](https://gitee.com/LongbowEnterprise/BootstrapAdmin) 项目为例详细介绍在 AppVeyor 服务中的操作步骤    

#### 注册账号
![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/121947_b304f51c_554725.png "Login.png")  

#### 关联项目  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/122448_04df5608_554725.png "Project.png")  
这里注意的是 AppVeyor 网站提供的免费服务仅限于公开仓库，如果小伙伴的仓库是私有，可以自己安装一下AppVeyor Server（非常小），操作相同。安装后就可以选择用户名密码或者 SSH Key 方式登录私有仓库了。（我搭建了个人私有AppVeyor Server，有需要的小伙伴可以在本文评论区索要登录名与密码，进行体验）  
关联码云仓库项目必须选择 Git 选项  

#### 项目设置
![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/123646_0a5dbd30_554725.png "Settings.png")  
新建项目后我们来到项目设置环节，由于页面设置比较多，下面我详细介绍一些关键的设置  

1. Git clone depth  
由于git clone 默认是整个仓库的 clone 如果仓库比较大，这个操作很耗时，`Git clone depth` 设置仅clone最近的提交，所以建议此处设置为 1  

2. Webhook URL  
此处我讲一下自动触发的原理，仓库接收到开发人员提交的代码后，会自动调用此处 webhook URL 地址以触发 AppVeyor 服务，在 BootstrapAdmin 仓库管理页面下的 WebHooks 面板中可以看到此处已经添加  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/161437_965dfbb7_554725.png "Gitee.png")  

3. Ignore `appveyor.yml`  
此设置对码云仓库无效，无论是否勾选均不起作用，仅对（GitHub, GitLab, VSTS, Kiln, BitBucket Server, GitHub Enterprise）生效，码云需要努力哦  
appveyor.yml 是什么？这个就是自动化非常重要的一个环节，可以认为是自动化执行配置脚本，不会写怎么办？不会写也不要紧，可以通过 AppVeyor UI 界面来配置也是可以的  

4. Custom configuration .yml file name  
由于上面一个设置无效，所以此处设置是 appveyor.yml 文件所在，设置为 https://gitee.com/LongbowEnterprise/BootstrapAdmin/raw/master/appveyor.yml  
为什么这样设置呢？因为 AppVeyor 服务是先 clone 代码再进行编译等动作，如果动作是依靠 appveyor.yml 脚本来执行的，未 clone 成功的时候是无法获取此脚本文件的，所以此处必须先告诉 AppVeyor 脚本文件所在位置，注意路径中我使用的是 **raw**   

其他的配置均默认即可，如 build 前、后、完成时执行什么脚本的设置  

#### 环境设置
![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/130134_cf7ac1d4_554725.png "Env.png")  
1. Build worker image  
此处设置是选择环境镜像，有多种环境可以选择，如 Ubuntu 环境， DOTNET 根据自己的版本不同选择不同的版本即可，由于 BootstrapAdmin 项目采用的是 net core 2.2 我这里选择 Visual Studio 2017  

2. Environment variables  
环境变量，有时我们需要初始化一些环境变量，如 BootstrapAdmin 项目需要将单元测试成功后的代码覆盖率结果上传到 http://coveralls.io 网站上做结果分析。需要设置登录该网站的 TOKEN ，所以此处我设置了 COVERALLS_REPO_TOKEN 这个环境变量，变量为加密后的该网站登录 Token  
如何加密 Token？   
点击顶部 Account 菜单，选中 Encrypt YAML 即可  
加密有什么用？  
防止 Token 直接暴露在脚本中  

3. Services
AppVeyor 提供了很多很多数据库服务供我们测试环节使用，我这里开启了三个数据库服务 SQL Server, MySql, MongoDB  

4. Cached directories and files  
AppVeyor 提供了缓存某个目录或者文件的功能，这个功能常用于默认提供的服务器没有我们想要的软件包或者依赖，我们可以通过命令安装或者下载到指定目录中，然后缓存，下次运行的时候就可以直接使用了  

5. Init script  
初始化脚本，此脚本在 clone 动作前执行  

6. Install script  
安装脚本，此脚本在 clone 动作之后执行  
在 BootstrapAdmin 项目中使用 MySql 数据库做单元测试，由于 MySql 默认的 my.ini 文件配置导致数据库中的中文均是乱码，所以 BootstrapAdmin 仓库中放入 my.ini 文件，在此处添加脚本如下  
```bash
xcopy "$($env:appveyor_build_folder)\DatabaseScripts\MySQL\my.ini" "C:\Program Files\MySQL\MySQL Server 5.7" /y
```
由于本脚本执行顺序在 clone 动作之后 Services 动作之前，所以提前把 my.ini 文件复制到 MySql 工作目录下  

#### Build  
此处配置比较简单，如果是 NET Framework 架构的直接使用 MSBuild 即可，非常简单方便  
如果是 NETCore 架构的需要选中 Script 选项，然后自己写 Powershell 命令行即可
```bash
dotnet build ./Bootstrap.Admin
dotnet publish ./Bootstrap.Admin
```

#### Test
如果是 NET Framework 架构的直接使用 Auto 配置即可  
如果是 NET Core 架构的需要选中 Script 选项，然后自己写 Powershell 命令行或者脚本即可  
```bash
./appveyor.test.ps1
```  
此处可以根据单元测试结果进行代码覆盖率的整合   
```bash
dotnet tool install coveralls.net --version 1.0.0 --tool-path "./tools"    
dotnet test UnitTest --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Include="[Bootstrap*]*" /p:ExcludeByFile="../Bootstrap.Admin/Program.cs%2c../Bootstrap.Admin/Startup.cs%2c../Bootstrap.Admin/HttpHeaderOperation.cs" /p:CoverletOutput=../
cmd.exe /c ".\tools\csmacnz.Coveralls.exe --opencover -i coverage.opencover.xml --useRelativePaths"
```  
第一行脚本通过命令行安装 converalls.net 工具到 ./tools 目录下  
第二行进行单元测试并且生成代码覆盖率结果
第三行通过 csmacnz.Coveralls.exe 命令将代码覆盖率结果文件上传到网站  

此处还可以判断单元测试结果，如果单元测试全部通过则开始构建 Docker 镜像
```bash
docker build -t reg.qiniu.com/argozhang/ba:master-db8dbbe
docker push reg.qiniu.com/argozhang/ba:master-db8dbbe
```

#### Artifacts
本配置可以收集一些需要打包的文件夹，或者文件，为发布做准备  
配置路径 Bootstrap.Admin\bin\release\netcoreapp2.2\publish\ 系统会自动将此文件夹下的所有文件打包到一个 zip 文件中  
配置路径 **\\*.nupkg 系统会将所有目录下的 nupkg 文件打包

#### Deployment
Appveyor 提供非常多的部署方式，我这就不详细解释了，大家选中一种适合自己的即可
我这里简单的介绍一下 FTP（WebDeploy）与 Nuget 方式  
FTP 与 Web Deploy 方式非常相似，都是填写主机 IP 地址，用户名与密码以及路径后，Artifacts填写上一步打包的文件名称即可。  
Nuget 方式非常流行。现在大部分 dll 的分发均采用 Nuget 方式了。  
Nuget server URL 配置为 https://www.nuget.org/api/v2/package  
API key 配置为自己在 http://www.nuget.org/ 网站的 Token   

#### Notifications
通知系统，Appveyor 提供了丰富的通知系统，此处我以 Email 配置为例
Recipients 填写收件人地址  
Subject 填写邮件标题，支持参数化配置  
Message 填写邮件正文，支持参数化配置  
Events 配置发送邮件消息事件，成功发送，失败发送，编译状态改变后发送 

以上是 Appveyor UI 配置全过程，更好的办法是配置好后通过网站提供的导出 `appveyor.yml` 功能，将配置导出到文件，保存到项目中  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/145550_956cc17e_554725.png " BA.png")   
有兴趣的小伙伴可以参考一下我的 [appveyor.yml](https://gitee.com/LongbowEnterprise/BootstrapAdmin/blob/master/appveyor.yml) 文件  

## 总结  
通过本篇文章介绍，小伙伴们可以通过清晰的了解到一下几个步骤：  
1. 提交代码到码云仓库
2. 码云 WebHooks 自动触发 AppVeyor 服务
3. Appveyor 开始按照 appveyor.yml 文件执行
4. 发布到 docker 中  

然后就可以在码云的项目中展示通过 AppVeyor 生成的精美小挂件展示其状态信息（点击可以查看明细）  

[![Appveyor build](https://img.shields.io/endpoint.svg?logo=appveyor&label=build&color=blueviolet&url=https%3A%2F%2Fargo.zylweb.cn%2FBA%2Fapi%2FGitee%2FBuilds?projName=bootstrapadmin-9m1jm)](https://ci.appveyor.com/project/ArgoZhang/bootstrapadmin-9m1jm)
[![Build Status](https://img.shields.io/appveyor/ci/ArgoZhang/bootstrapadmin-9m1jm/master.svg?logo=appveyor&label=maser)](https://ci.appveyor.com/project/ArgoZhang/bootstrapadmin-9m1jm)
[![Test](https://img.shields.io/appveyor/tests/ArgoZhang/bootstrapadmin-9m1jm/master.svg?logo=appveyor&)](https://ci.appveyor.com/project/ArgoZhang/bootstrapadmin-9m1jm/build/tests)

最后放上一张完整的 Appveyor 输出界面  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0604/145824_0d5542a6_554725.png "Console.png")  
